#include <iostream>
using namespace std;



//Sommet
struct GraphNode {
    int value;
    Edge* edges;

    void appendNewEdge(GraphNode* destination, int distance) {
        Edge* arete = new Edge;
        arete->source = this;
        arete->destination = destination;
        arete->distance = distance;

        Edge* first = this->edges;
        
        while(first != nullptr) {
            first = first->Next;
        }

        first->Next = arete;
        
    }
};


//Arête
struct Edge {
    GraphNode* source;
    GraphNode* destination;
    int distance;
    Edge* Next;
};

//Graphe
struct Graph {
    int size = 256;
    int nodecount = 0;
    GraphNode* tab[256];

    void appendNewNode(GraphNode* node) {
        this->tab[nodecount] = node;
        this->nodecount++;
    }

    void printGraph() {
        cout << "(" ;
        for (int i=0; i<this->nodecount-1; i++) {
            cout << this->tab[i]->value << ", ";
        }
        cout << this->tab[this->nodecount]->value << ")";
    }

};



Graph buildFromAdjacenciesMatrix(int** adjacencies, int nodeCount) {
    Graph G;

    return G;
}