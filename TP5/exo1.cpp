#include "exo1.h"

int main() {
    HashTable* Table = new HashTable;
    Table->size = 5;
    string mots[] = {"Cc", "Alo", "Yolo", "Manger", "Ah"};
    Table->table = mots;

    Table->display_table();
    cout << Table->hashVal(Table->table[0]) << endl;

    Table->insert("Bonjour");
    Table->display_table();
    cout << Table->contains("Bonjour") << endl;
    cout << Table->contains("Alo") << endl;

}