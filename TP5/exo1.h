#include <iostream>
using namespace std;


struct HashTable {
    int size;
    string* table; // Tableau des string

    void display_table() {
        cout << "{" ;
        for (int i=0; i<this->size-1; i++) {
            cout << this->table[i] << ", " ;
        }
        cout << this->table[size-1];
        cout << "}" << endl;
    }
    

    // Hash : retourne valeur de hachage de word (size = taille du tableau)
    int hashVal(string word) {
        
        if ((int)word[0] >= this->size) {
            return (int)word[0] % this->size;
        }

        return (int)word[0];
    }

    // Insert word en hashVal(word)-ième position
    void insert(string word) {
        this->table[hashVal (word)] = word;
    }


    bool contains(string word) {
        for (int i=0; i<this->size; i++) {
            if (this->table[i] == word) {
                return true;
            }
        }
        return false;
    }
    
} ;




