#include <iostream>
using namespace std;


struct Map {

    string chaine;
    int val;

    Map* left = nullptr;
    Map* right = nullptr;

    void display_map() {

        if (this==nullptr) {
            cout << "Arbre vide." << endl;
        }

        else {

            cout << this->chaine << ", " << this->val << " (HashCode = " << hashPolyVal(this->chaine) << ")" <<  endl;
        
            if (this->left != nullptr) {
                cout << " -- Gauche de " << this->chaine << " = " << this->left->chaine << ", " << this->left->val << " (HashCode = " << hashPolyVal(this->left->chaine) << ")" << endl;
                (this->left)->display_map();
            }

            if (this->right != nullptr) {
                cout << " -- Droite de " << this->chaine << " = " << this->right->chaine << ", " << this->right->val << " (HashCode = " << hashPolyVal(this->right->chaine) << ")" <<  endl;
                (this->right)->display_map();
            }

            if ((this->left == nullptr) || (this->right == nullptr)) {
                cout << "Fin branche de "  << this->chaine << endl;
            }
        }

    }

    // Hash : retourne valeur de hachage polynomial du string key
    int hashPolyVal(string key) {
        int hashval=0;
        int puiss=1;

        for (int i=key.size()-1; i>=0; i--) {
            hashval+= ((int) key[i])*puiss;
            puiss*=256;
        }

        return hashval;
    }

    void insert(string key, int value) {
        // création du noeud
        Map* Noeud  = new Map;
        Noeud->chaine = key; 
        Noeud->val = value;

        if (hashPolyVal(key) < hashPolyVal(this->chaine)) { // Gauche
            if (this->left == nullptr) {
                this->left = Noeud;
            }

            else {
                (this->left)->insert(key, value);
            }
        }

        else { // Droite
            if (this->right == nullptr) {
                this->right = Noeud;
            }

            else {
                (this->right)->insert(key, value);
            }
        }

    }

    int get(string key) { 

        if (this->chaine == key) {
            return this->val;
        }

        if (hashPolyVal(key) < hashPolyVal(this->chaine)) {
            return this->left->get(key);
        }

        else {
            return this->right->get(key);
        }
        
        return 0;
    
    }
    
} ;

