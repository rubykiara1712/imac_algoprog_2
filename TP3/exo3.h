#include <iostream>
using namespace std;

// structure Arbre Binaire
struct binaryTree {
    int val;

    binaryTree* left = nullptr;
    binaryTree* right = nullptr;


// Un affichage de l'arbre
    void print() {

        if (this==nullptr) {
            cout << "Arbre vide." << endl;
        }

        else {

            cout << this->val << endl;
        
            if (this->left != nullptr) {
                cout << " -- Gauche de " << this->val << " = " << this->left->val << endl;
                (this->left)->print();
            }

            if (this->right != nullptr) {
                cout << " -- Droite de " << this->val << " = " << this->right->val << endl;
                (this->right)->print();
            }

            if ((this->left == nullptr) || (this->right == nullptr)) {
                cout << "Fin branche de "  << this->val << endl;
            }
        }

    }


// Insère noeud contenant value dans l'arbre
    void insertNumber(int value) {
        
        // création du noeud
        binaryTree* Noeud  = new binaryTree;
        Noeud->val = value; 

        if (value < this->val) { // Gauche
            if (this->left == nullptr) {
                this->left = Noeud;
            }
            else {
                (this->left)->insertNumber(value);
            }
        }

        else { // Droite
            if (this->right == nullptr) {
                this->right = Noeud;
            }
            else {
                (this->right)->insertNumber(value);
            }
        }

    }


// Retourne la hauteur de l'arbre
    int height() {
        int hg=0; // hauteur gauche
        int hd=0; // hauteur droite

        if (this->left != nullptr) {
            hg=hg+this->left->height(); // récursif : on descend
        }
        if (this->right != nullptr) {
            hd=hd+this->right->height();
        }

        // on retourne le max des 2 (+1 qui est la racine)

        if (hd>hg) {
            return hd+1;
        }
        else {
            return hg+1;
        }       
    }



// Retourne le nb de noeuds de l'arbre
    int nodesCount() {
        int hg=0; // hauteur gauche
        int hd=0; // hauteur droite

        if (this->left != nullptr) {
            hg=hg+this->left->nodesCount(); // récursif : on descend
        }
        if (this->right != nullptr) {
            hd=hd+this->right->nodesCount();
        }

        // on retourne la somme des 2 (+1 qui est la racine)

        return (hg + hd + 1);   
    }

// Retourne vrai si feuille, faux si branche (ccomplète ou non)
    bool isLeaf() {
        return ((this->left == nullptr) && (this->right == nullptr));
    }

// Remplit avec les feuilles de l'arbre
    int allLeaves(binaryTree* leaves[], int* leavesCount) {
        

        if (this->isLeaf()) {
            *leavesCount+=1;
            leaves[*leavesCount]=this;
        }
        if (this->right != nullptr) {
            this->right->allLeaves(leaves, leavesCount);
        }
        if (this->left != nullptr) {
            this->left->allLeaves(leaves, leavesCount);
        }

    return *leavesCount;

    }

// INFIXE - Left, Parent, Right
    int inorderTravel(binaryTree* nodes[], int* nodesCount) {

        if (this->left != nullptr) {
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[*nodesCount]=this;
        *nodesCount+=1;

        if (this->right != nullptr) {
            this->right->inorderTravel(nodes, nodesCount);
        }

    return *nodesCount;

    }


// PREFIXE - Parent, Left, Right
    int preorderTravel(binaryTree* nodes[], int* nodesCount) {
        nodes[*nodesCount]=this;
        *nodesCount+=1;

        if (this->left != nullptr) {
            this->left->inorderTravel(nodes, nodesCount);
        }


        if (this->right != nullptr) {
            this->right->inorderTravel(nodes, nodesCount);
        }

    return *nodesCount;

    }

// POSTFIXE - Left, Right, Parent
    int postorderTravel(binaryTree* nodes[], int* nodesCount) {

        if (this->left != nullptr) {
            this->left->inorderTravel(nodes, nodesCount);
        }

        if (this->right != nullptr) {
            this->right->inorderTravel(nodes, nodesCount);
        }

        nodes[*nodesCount]=this;
        *nodesCount+=1;

    return *nodesCount;

    }

    binaryTree* search(int value) { 
        if (this->val == value) {
            return this;
        }

        if (value < this->val) {
            return this->left->search(value);
        }

        else {
            return this->right->search(value);
        }
        
        binaryTree* ArbreVide = new binaryTree;
        return ArbreVide;
    
    }

};





