#include <iostream>
using namespace std;


// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}


// recherche dichotomique des index extremes (min et max) de l'entier toSearch dans le tableau tab (TRIÉ)
void binarySearchAll(int* tab, int n, int toSearch, int* imin, int* imax) {    
    
    int IMIN=-1;
    int IMAX=-1;

    int start = 0;
    int end = n;
    int mid;

    while (start<end) {

        mid = (start+end)/2; // on divise par 2 la longueur du tableau

        if (toSearch >= tab[mid]) { // à droite
            start = mid+1;
        }

        else { // à gauche
            end = mid;
        }

        break;

    }

    IMIN = start;

    start = 0;
    end = n; 

    while (start<end) {
        mid = (start+end)/2; // on divise par 2 la longueur du tableau

        if (toSearch >= tab[mid]) { // à droite
            start = mid+1;
        }

        else { // à gauche
            end = mid;
        }
        
        break;

    }

    IMAX = end-1;
    

    // on réaffecte *imin et *imax du main
    *imin = IMIN;
    *imax = IMAX;

}



int main() {
    int tableau[7] = {1,2,5,6,7,7,7};
    display_tab(tableau, 7);

    int imin;
    int imax;

    binarySearchAll(tableau, 7, 7, &imin, &imax);

    cout << "7 est en positions : " << endl << "imin : "  << imin << endl;
    cout << "imax : "  << imax << endl;

}