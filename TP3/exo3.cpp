#include "exo3.h"


int main() {

    // Test insertion & affichage
    binaryTree* ARBRE = new binaryTree;
    ARBRE->val = 3;


    ARBRE->insertNumber(5);
    ARBRE->insertNumber(2);
    ARBRE->insertNumber(15);
    ARBRE->insertNumber(6);
    ARBRE->insertNumber(8);
    ARBRE->insertNumber(18);
    ARBRE->insertNumber(41);
    ARBRE->insertNumber(-1);

    cout << "Affichage de ARBRE : " << endl;
    ARBRE->print();

    cout << endl << "Hauteur de ARBRE = " << ARBRE->height() << endl ;
    cout << endl << "Nb de noeuds de ARBRE = " << ARBRE->nodesCount() << endl ;

    binaryTree* ARBRELEAVES[256];
    int L=0;

    cout << "allLeaves contient " << ARBRE->allLeaves(ARBRELEAVES, &L) << " feuilles après appel." << endl;

    binaryTree* ARBRENODES[256];
    int L2=0;
    
    cout << "nodes contient " << ARBRE->postorderTravel(ARBRENODES, &L2) << " noeuds après appel." << endl;

    ARBRE->search(15)->print();

    return 0;
}

