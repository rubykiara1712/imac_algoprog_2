#include <iostream>
using namespace std;

void display_tab(int* tab, int n);
int binarySearch(int* tab, int n, int toSearch);

int main() {
    int tableau[7] = {1,2,5,6,6,7,7};
    display_tab(tableau, 7);
    cout << "7 est en position : " ;
    cout << binarySearch(tableau, 7, 7) << endl;
}


// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}

// recherche dichotomique de l'index de l'entier toSearch dans le tableau tab (TRIÉ)
int binarySearch(int* tab, int n, int toSearch) {

    int start = 0;
    int end = n;
    int mid = 0; 

    while (start<end) {
        mid = (start+end)/2; // on divise par 2 la longueur du tableau

        if (toSearch > tab[mid]) { // à droite
            start = mid+1;
        }
        else if (toSearch < tab[mid]) { // à gauche
            end = mid;
        }
        else { // si tab[mid] = toSearch, on retourne l'index = mid
            return mid;
        }
    }
    return -1; // Si non trouvé
}
