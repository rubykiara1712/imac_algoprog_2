// TP 1 Wendy GERVAIS - 11/02/22
// Fonctions Récursives

#include "exo1_2_3_4_5.h"

int main () {
    // Tests des différentes fonctions

    //Affichage 5^2
    cout << power(5,2) << endl;

    //Affichage des 10 premiers nombres de Fibonacci
    for (int i=0; i<10; i++) {
        cout << fibonacci(i) << endl;
    }
    //Recherche de 5 dans tab = {1,5,2,3,4}
    int tab[5]={1,5,2,3,4};
    cout << search(5, tab, 5) << endl;

    //Affichage des pairs dans un array {1,2,4,1,3}
    int array[5]={1,2,4,1,3};
    int evens[5]={};
    allEvens(evens, array, 0, 5);
    for (int j=0; j<5; j++) {
        cout << evens[j] << endl;
    }


    // test Mandelbrot
    vec2 z; //creation d'un complexe z
    z.x = 0;
    z.y = 0;

    vec2 point; //creation d'un point
    point.x = 9.0;
    point.y = 7.0;
    int n = 15;

    bool rep = mandelbrot(z, point, n);
    if (rep) {
        cout << "Vrai" << endl;
    }

    if (!rep) {
        cout << "Faux" << endl;
    }

}



// Ex1 : Retourne value à la puissance n
int power(int value, int n) {
    if (n==0) {
        return 1;
    }
    else if (n==1) {
        return value;
    }
    else {
        return (power(value, n-1))*value;
    }
}

// Ex2 : Retourne le n-ième Fibonacci
int fibonacci(int n) {
    if (n<=1) { //condition d'arrêt
        return n;
    }
    return (fibonacci(n-1)+fibonacci(n-2));
}

// Ex3 : Retourne l'index de value dans le tableau array
int search(int value, int array[], int size) {
    if (size==0) { //condition d'arrêt
        return -1;
    } 
    if (array[size-1]==value) {
        return size-1;
    }
    return search(value, array, size-1);
}

// Ex4 : Remplit evens avec les nb pairs de array
int allEvens(int evens[], int array[], int evenSize, int arraySize) {
    if (arraySize==0) { //condition d'arrêt
        return -1;
    }
    if (array[arraySize-1]%2==0) {
        evens[evenSize] = array[arraySize-1];
        return allEvens(evens, array, evenSize+1, arraySize-1);
    }
    return allEvens(evens, array, evenSize, arraySize-1); //recursif sur arraySize-1
}


// Ex5 : Retourne vrai si point appartient à l'ensemble de Mandelbrot de z
bool mandelbrot(vec2 z, vec2 point, int n) {
    vec2 zcarre; // calcul de z2
    zcarre.x = (z.x)*(z.x)- (z.y)*(z.y);
    zcarre.y = (2*z.x)*(z.y);

    vec2 fdez; // calcul de f(z)
    fdez.x = zcarre.x + point.x;
    fdez.y = zcarre.y + point.y;

    if (n==0) { //condition d'arrêt
        return false;
    }

    float normef = sqrt(z.x*z.x + z.y*z.y); // norme de z

    return (normef < 2 || mandelbrot(fdez, point, n-1));


}