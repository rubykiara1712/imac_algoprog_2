// TP 1 Wendy GERVAIS - 11/02/22
// Fonctions Récursives (Header)

#include <iostream>
#include <cmath>
#include <math.h>

using namespace std;

// Structure vecteur exo 5
struct vec2 {
    float x;
    float y;
} ;

// Prototypage des fonctions
int power(int value, int n);
int fibonacci(int n);
int search(int value, int array[], int size);
int allEvens(int evens[], int array[], int evenSize, int arraySize);
bool mandelbrot(vec2 z, vec2 point, int n);

