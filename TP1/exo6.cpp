// TP 1 Wendy GERVAIS - 13/02/22
// Ex 6 : DynaTableau, Liste

#include "exo6.h"

// DynaTableau = DT
// Liste (chaînée) = LC

int main() {

    // Test avec un DynaTableau
    DynaTableau montablo;
    montablo.nb = 5;
    montablo.taillemax=5;
    int cc[5] = {1,5,6,7,7};
    montablo.tab = cc;

    // Premier affichage
    cout << "Avant ajoutage : " << endl;
    for (int i=0; i<montablo.nb; i++) {
        cout << montablo.tab[i] << endl;
    }

    // Ajoutage
    ajouteDT(24, &montablo);

    // Affichage 2
    cout << "Après ajoutage : " << endl;
    for (int i=0; i<montablo.nb; i++) {
        cout << montablo.tab[i] << endl;
    }

    cout << "Element en index 1 dans {1,5,6,7,7} : " << recupereDT(1, montablo) << endl;

    return 0;
}



// Ajout d'un élément dans le DT
void ajouteDT(int valeur, DynaTableau* Dtab) {

    if (Dtab->nb < Dtab->taillemax) { //S'il reste de la place
        Dtab->tab[Dtab->nb]=valeur; //j'ajoute la valeur en nb-ieme position
        Dtab->nb++;
    }

    else { // si déjà rempli
        int* tab2 = new int[Dtab->taillemax+1]; //je crée un tableau plus grand

        for (int i=0; i<(Dtab->taillemax); i++) {
            tab2[i] = Dtab->tab[i]; //je remplis ce tableau avec les éléments du premier tableau
        }

        tab2[Dtab->taillemax] = valeur; // le dernier élément est la valeur demandée
        (Dtab->tab) = tab2; // je mets ce nouveau tableau dans la structure

        Dtab->nb++; // j'incrémente le nb d'éléments du tableau
        Dtab->taillemax ++; //j'augmente la taille de 1
      
    }



}

// Ajout d'un élément dans la LC
void ajouteLC(int valeur, Liste* tab) {
    Liste* suiv = new Liste; // Nouvelle cellule dans la chaine
    suiv->val = valeur; // Affectation de la valeur
    tab->next = suiv; // Pointe sur la cellule suivante
}


// Renvoie le n-ième élément du DT
int recupereDT(int n, DynaTableau Dtab) {
    return (Dtab.tab[n]);
}

// Renvoie le n-ième élément de la LC
int recupereLC(int n, Liste tab) {
    if (n==0) { // condition d'arrêt
        return tab.val;
    }
    else {
        return (recupereLC(n-1, *(tab.next))); // recursif car chaine
    }
}

// Renvoie l'index de n dans le DT
int chercheDT(int n, DynaTableau Dtab) {
    for (int i=0; i<Dtab.nb; i++) { // Parcourt le DT
        if (Dtab.tab[i] == n) { // si le i-ème est n
            return i; // on retourne l'index
        }
    }
    return -1; // si non trouvé
}

// Renvoie l'index de n dans la LC
int chercheLC(int n, Liste* tab) {
    int index=0;

    while (tab != nullptr) {

        if ((tab->val)==n) {
            return index;
        }

        index++;
        tab = tab->next;
    }

    return -1;

}

// Redéfinit la n-ième valeur du DT en valeur
void stockeDT(int n, int valeur, DynaTableau * Dtab) {
    Dtab->tab[n] = valeur;

}

// Redéfinit la n-ième valeur de la LC en valeur
void stockeLC(int n, int valeur, Liste *tab) {
    int index=0;

    while (tab != nullptr) {
        if (index==n) {
            tab->val = valeur;
        }

        index++;
        tab = tab->next;
    }
}
