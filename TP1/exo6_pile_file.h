
// TP 1 Wendy GERVAIS - 21/02/22
// Ex 6 : Piles, Files (Header)

#include <iostream>
using namespace std;

struct pile {
    int container[10];
    int count=0;
};

struct file {
    int container[10];
    int count=0;
};

bool pousser_pile(pile* p, int x);
int retirer_pile(pile* p);

bool pousser_file(file* p, int x);
int retirer_file(file* p);