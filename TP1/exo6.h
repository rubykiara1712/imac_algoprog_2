// TP 1 Wendy GERVAIS - 13/02/22
// Ex 6 : DynaTableau, Liste (Header)

#include <iostream>
#include <cmath>
#include <math.h>

using namespace std;

// DynaTableau = DT
// Liste (chaînée) = LC

//Structures DynaTableau et Liste
struct DynaTableau {
    int nb;
    int taillemax;
    int* tab;
} ;

struct Liste {
    int val;
    Liste *next;
} ;


//Prototypage des fonctions
void ajouteDT(int valeur, DynaTableau* Dtab);
void ajouteLC(int valeur, Liste* tab);

int recupereDT(int n, DynaTableau Dtab);
int recupereLC(int n, Liste tab);

int chercheDT(int n, DynaTableau Dtab);
int chercheLC(int n, Liste* tab);

void stockeDT(int n, int valeur, DynaTableau * Dtab);
void stockeLC(int n, int valeur, Liste *tab);


