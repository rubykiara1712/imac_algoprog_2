
// TP 1 Wendy GERVAIS - 21/02/22
// Ex 6 : Piles, Files
#include "exo6_pile_file.h"


int main() {

    // Petit test sur pile et file
    pile p;
    pousser_pile(&p, 4);

    file f;
    pousser_file(&f, 4);

}


bool pousser_file(file *f, int x) {
    if (f->count==10) {
        for (int i=0; i<f->count-1; i++){
            f->container[i] = f->container[i+1];
        }
    }
    f->container[f->count-1]=x;
    f->count++;
    return true;

}

int retirer_file(file *f) {
    int lastelement = f->container[f->count];
    f->container[f->count] = 0;
    f->count--;
    return lastelement;
}


bool pousser_pile(pile *p, int x) {
    if (p->count==10) {
        return false;
    }
    else {
        p->container[p->count+1] = x;
        p->count++;
        return true;
    }

}

int retirer_pile(pile *p) {
    int lastelement = p->container[p->count];
    p->container[p->count] = 0;
    p->count--;
    return lastelement;
}


