#include <iostream>
using namespace std;

// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}


struct HuffmanNode {

    int value; //  la fréquence d’apparition de ce caractère
    char character; // le caractère représentant le noeud
    string code=""; // et le code produit par le dictionnaire

    HuffmanNode* left=nullptr;
    HuffmanNode* right=nullptr;
    
    void printNode() {

        cout << "Valeur : " << this->value << " / Character : " << this->character << " / Code : " << this->code << endl;

        if (this->left != nullptr) {
            cout << " <-- Enfant gauche :  " << endl;
            this->left->printNode();
            cout << endl;
        }

        if (this->right != nullptr) {
            cout << " --> Enfant droit : " << endl;
            this->right->printNode();
            cout << endl;
        } 


    }

    // Détermine et définit les codes de toutes les feuilles de tree
    void processCodes(string baseCode) {


        // Si feuille : "baseCode"
        if (this->left==nullptr && this->right==nullptr) {
            this->code += baseCode;
        }

        else { // Récursif
            // Gauche : .'0'
            if (this->left != nullptr) {
                this->left->processCodes(baseCode+'0');
            }

            // Droite : .'1'
            if (this->right != nullptr) {
                this->right->processCodes(baseCode+'1');
            }
        }
    }

} ;


struct HuffmanHeap {

    int size = 0;
    HuffmanNode heap[256];

    void print(){
        for (int i=0; i<this->size; i++) {
            cout << i << "-ième Node : ";
            this->heap[i].printNode();
            cout << endl;
        }
    }

    // Retourne l'index de l'enfant gauche (2i+1 ou -1 si n'existe pas)
    int leftChild(int nodeIndex) {
        if (nodeIndex*2 + 1 < this->size) {
            return (nodeIndex*2 + 1);
        }
        return -1;
    }

    // Retourne l'index de l'enfant droit (2i+2 ou -1 si n'existe pas)
    int rightChild(int nodeIndex) {
        if (nodeIndex*2 + 2 < this->size) {
            return (nodeIndex*2 + 2);
        }
        return -1;
    }

    void swap(HuffmanNode* a, HuffmanNode* b) {
        HuffmanNode* c = new HuffmanNode;
        *c = *a;
        *a = *b;
        *b = *c;
        delete c;
    }


    // Insère un nouveau HuffmanNode dans le tas en utilisant frequence comme priorité. 
    // Le nouveau noeud est constitué de character et frequence.
    void insertHeapNode(char caractere, int frequence){
        
        // on commence par la fin du tas
        int i = this->size;
        
        this->heap[i].value = frequence;
        this->heap[i].character = caractere;
        
        // tant qu'il reste des noeuds à parcourir et que l'enfant est supérieur au parent
        while(i>0 && this->heap[i].value > this->heap[(i-1)/2].value) {
            
            // on échange parent & enfant
            swap(&this->heap[i],&this->heap[(i-1)/2]);

            // on passe au parent suivant
            i = (i-1)/2;
        }

        this->size++;
    }
} ;



// Remplit le tableau frequences avec l'occurrence de chaque caractère de data, en position correspondant à son code ASCII
void charFrequences(string data, int* frequences) {
    int c = 0;
    while(data[c] != '\0') {
        int codeASCII = (int) data[c];
        frequences[codeASCII]++;
        c++;
    }
}


// Construit un tas heap minimum à partir des fréquences d’apparition non nulles de caractères
void huffmanHeap(int* frequences, HuffmanHeap* heap) {
    for (int i=0; i<=128; i++) {
        if (frequences[i]!=0) {
            heap->insertHeapNode((char) i, frequences[i]);
        }
    }
}


// Insère un nouveau noeud dans tree
void insertNode(HuffmanNode* tree, HuffmanNode* node) {

    // Si feuille
    if (tree->left==nullptr && tree->right==nullptr) {

        // copy de tree
        HuffmanNode* copy = new HuffmanNode;
        copy->value = tree->value;
        copy->character = tree->character;
        copy->code=tree->code;


        if (node->value <= 2*copy->value) {
            tree->left = node;
            tree->right = copy;
        }

        else if (node->value > 2*copy->value) {
            tree->right = node;
            tree->left = copy;
        }

        tree->character = '\0';
        
    }


    else {
        if (node->value <= 2*tree->value) {
            insertNode(tree->left, node);
        } 

        else {
            insertNode(tree->right, node);
        }
        
    }

    tree->value += node->value;
    
}
    
    

// Construit un dictionnaire de Huffman : heap->tree 
void huffmanDict(HuffmanHeap* heap, HuffmanNode* tree)  {

    for (int i=0; i<heap->size; i++) {
        HuffmanNode* node = new HuffmanNode;
        node->value = (heap->heap[i].value);
        node->character = (heap->heap[i].character);

        insertNode(tree, node);
        
    }






}

// Retourne la chaine de caractère encodé à partir des codes se trouvant dans characters. 
// characters est un tableau de HuffmanNode*, chaque indice i correspond au code ASCII i.
void huffmanEncode(HuffmanNode** characters, string toEncode) {

}

// Retourne la chaine de caractère décodé partir du dictionnaire dict
void huffmanDecode(HuffmanNode* dict, string toDecode) {

}


