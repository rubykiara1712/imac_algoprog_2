#include <iostream>
using namespace std;

// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}

// Fonction qui échange les VALEURS de deux entiers a et b
void swap(int* a, int* b) {
    int* c = new int;
    *c = *a;
    *a = *b;
    *b = *c;
    delete c;
}


// structure Tas
struct Heap {
    int size;
    int* heap;

    void print() {
        display_tab(this->heap, size);
    }

    // Retourne l'index de l'enfant gauche (2i+1 ou -1 si n'existe pas)
    int leftChild(int nodeIndex) {
        if (nodeIndex*2 + 1 < this->size) {
            return (nodeIndex*2 + 1);
        }
        return -1;
    }

    // Retourne l'index de l'enfant droit (2i+2 ou -1 si n'existe pas)
    int rightChild(int nodeIndex) {
        if (nodeIndex*2 + 2 < this->size) {
            return (nodeIndex*2 + 2);
        }
        return -1;
    }

    void insertHeapNode(int value) {
        // on commence par la fin du tas, on y met la value
        int i = this->size;
        
        (this->heap[i]) = value;

        // tant qu'il reste des noeuds à parcourir et que l'enfant est supérieur au parent
        while(i>0 && this->heap[i] > this->heap[(i-1)/2]) {
            // on échange parent & enfant
            swap(this->heap[i], this->heap[(i-1)/2]);

            // on passe au parent suivant
            i = (i-1)/2;
        }
        this->size ++;
    }

    // Mise à jour d'un tas de taille n à partir d'un noeud i
    void heapify(int i) {

    // On recupère l'indice du max = largest
        int largest = i;

        int r = rightChild(i);
        int l = leftChild(i);

        if (l != -1 && this->heap[l] > this->heap[largest]) {
            largest = l;
        }

        if (r != -1 && this->heap[r] >= this->heap[largest]) {
            largest = r;
        }

        // On swap avec le largest
        if (largest != i) {
            swap(&this->heap[i], &this->heap[largest]);
            this->heapify(largest);
        }
       
    }

    // Heapify sur le heap total : transforme un tableau "random" en heap
    void BuildHeap() {
        int start = ((this->size)/2);
        for (int k=start; k>=0; k--) {
            this->heapify(k);
        }

    }

    // Transforme un tableau heap en tableau trié
    void heapSort(){
        int n = this->size;
        for (int i=this->size-1; i>0; i--) {
            // on swap en parcourant le heap 
            swap(&this->heap[0], &this->heap[i]);
            this->size--;
            this->heapify(0);
        }

        this->size = n;

    }

};
