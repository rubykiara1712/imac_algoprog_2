#include "exo2.h"

int main() {

    // on initialise frequences avec que des 0
    int frequences[128];
    for (int i=0; i<=127; i++) {
        frequences[i]=0;
    }

    string phrase = "YOLO";
    charFrequences(phrase, frequences);
   
    HuffmanHeap* HEAP = new HuffmanHeap;
    huffmanHeap(frequences, HEAP);
    HEAP->print();

    cout << "Taille de heap : " << HEAP->size << endl << endl;

    HuffmanNode* TREE = new HuffmanNode;
    huffmanDict(HEAP, TREE);


    TREE->processCodes("");
    TREE->printNode();



    delete HEAP;
    delete TREE;

    return 0;
}