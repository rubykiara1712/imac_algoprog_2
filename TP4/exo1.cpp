#include "exo1.h"

int main() {
    Heap* TAS = new Heap;

    cout << "Tableau original random ----- " << endl;
    TAS->size = 11;
    int tab[256] = {1, 3, 5, 4, 6, 13, 10, 9, 8, 15, 17};

    TAS->heap=tab;
    TAS->print();

    cout << "Tableau heapifié ----- " << endl;
    TAS->BuildHeap();
    TAS->print();

    cout << "Tableau heapifié + trié ----- " << endl;
    TAS->heapSort();
    TAS->print();

    return 0;
}