#include <iostream>
using namespace std;

void display_tab(int* tab, int n);
void insertion_sort(int* tab, int n);


int main() {
    int tableau[6] = {5,2,8,7,15,2};
    display_tab(tableau, 6);

    insertion_sort(tableau,6);
    display_tab(tableau, 6); 
    // {2, 2, 5, 7, 8, 15}

}



// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}


// Tri par insertion
void insertion_sort(int* tab, int n) {
    bool dansIf = false; // booléen de contrôle boucle if

    int tabtrie[n]; // tableau result
    tabtrie[0]=tab[0]; // init premier élément
    int nt = 1; // taille du tableau result

    for (int i=1; i<n; i++) { // on parcourt tab
        dansIf = false;
        
        for (int j=0; j<nt; j++) { //on parcourt les éléments déjà dans tabtrie (de i à nt)

            
            if (tabtrie[j] > tab[i]) { //si l'élément courant du tableau trié (jième) est plus grand que l'élément du tableau d'origine

                for (int k=nt; k>j; k--) {
                    tabtrie[k] = tabtrie[k-1]; // on décale les éléments après
                } 
                
                tabtrie[j] = tab[i]; // on réaffecte la valeur

                dansIf = true;
                break;
            }
        }

        nt++;

        if (!dansIf) { // "else" : si on est pas passé dans la boucle if
            tabtrie[nt-1] = tab[i]; // on ajoute simplement l'élément à la fin
        }
    
    }

    for (int l=0; l<n; l++) { // on réaffecte tab pour qu'il soit le tableau trié à l'issue de l'appel (index l par index l)
        tab[l] = tabtrie[l];
    }

}