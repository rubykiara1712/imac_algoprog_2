#include <iostream>
using namespace std;

void display_tab(int* tab, int n);
void quick_sort(int* tab, int n);


int main() {
    int tableau[6] = {5,2,8,1,15,1};
    display_tab(tableau, 6);

    quick_sort(tableau, 6);
    display_tab(tableau, 6); 
    // {2, 2, 5, 7, 8, 15}

}

// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}



// Tri rapide
void quick_sort(int* tab, int n) {

    if (n<=1) { // condition d'arrêt : tableau assez petit = déjà trié
        return;
    }

    int pivot = tab[0]; 

    int lowers[n];
    int nl=0;

    int greaters[n];
    int ng=0;

    for (int i=1; i<n; i++) { // on parcourt la liste après le pivot

        if (tab[i]<=pivot) { // on a une valeur inférieure au pivot donc on l'insère dans le tableau lower
            lowers[nl]=tab[i];
            nl++; // on augmente alors la taille de lower
        }

        else { // sinon même procédé pour le tableau greaters
            greaters[ng]=tab[i];
            ng++;
        }
        

    }

    // on travaille récursivement sur lowers et greaters pour les trier
    quick_sort(lowers, nl);
    quick_sort(greaters, ng);

    // on les fusionne dans tab

    for (int k=0; k<nl; k++) {
        tab[k]=lowers[k];
    }

    tab[nl] = pivot;

    for (int k=0; k<ng; k++) {
        tab[k+nl+1]=greaters[k];
    }

    return;

}