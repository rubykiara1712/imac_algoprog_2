#include <iostream>
using namespace std;

void display_tab(int* tab, int n);
void bubble_sort(int* tab, int n);


int main() {
    int tableau[6] = {5,2,8,1,15,1};
    display_tab(tableau, 6);

    bubble_sort(tableau,6);
    display_tab(tableau, 6); 
    // {2, 2, 5, 7, 8, 15}

}

// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}


// Tri à bulle
void bubble_sort(int* tab, int n) {
    bool estTrie=true; //variable de contrôle : sera true si le tableau est trié (ie en n passages aucun échange de valeurs n'est fait) et alors on arrête la fonction

    int tabj; // variable intermédiaire qui sera utile pour échanger valeurs du tableau

        for (int i=0; i<n; i++) { // on effectue n fois la démarche
            estTrie=true;
            
            for (int j=0; j<n-1; j++) { // on parcourt la liste 
                if (tab[j+1] < tab[j]) { // on compare deux éléments succesifs ; si le second est plus grand, on les échange :

                    tabj = tab[j];
                    tab[j]=tab[j+1];
                    tab[j+1]=tabj;
                    
                    estTrie=false; // on a fait une permutation ; le tableau n'est pas trié

                }
            }
        }

        if (estTrie) {
            return; // on interrompt la fonction
        }
    


}