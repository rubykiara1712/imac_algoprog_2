#include <iostream>
using namespace std;

void display_tab(int* tab, int n);

int* merge(int* tab1, int n1, int* tab2, int n2);
int* merge_sort(int* tab, int n);


int main() {
    int tableau[6] = {5,2,8,1,15,1};
    display_tab(tableau, 6);
    display_tab(merge_sort(tableau, 6), 6); 
    // {1, 2, 5, 7, 8, 15}
}


// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}



// Fonction qui fusionne 2 tableaux tab1 et tab2 (resp. de tailles n1 et n2)
int* merge(int* tab1, int n1, int* tab2, int n2) {
    
    int i = 0; // compteur tab1
    int j = 0; // compteur tab2

    int* tab = new int; // tableau qui sera la fusion
    int k = 0; // compteur tab

    // on parcourt tab1 et tab2 (le premier sur i, le second sur j)
    while (i<n1 && j<n2) { 

        // on ajoute à tab le plus petit entre tab1[i] et tab2[j]
        if (tab1[i] < tab2[j]) { 
            tab[k] = tab1[i];
            i++;
            k++;
        }

        else {
            tab[k] = tab2[j];
            j++;
            k++;
        }
    }

    // tab devient : tab + tab1 après i + tab2 après j
    for (int i1=i; i1<n1; i1++) {
        tab[k] = tab1[i1];
        k++;
    }

    for (int j1=j; j1<n2; j1++) {
        tab[k] = tab2[j1];
        k++;
    }

    return tab;
    // on renvoie le tableau fusionné

}


// Tri fusion
int* merge_sort(int* tab, int n) {


    if (n <= 1) { // condition d'arrêt : tableau de taille 0 ou 1 (déjà trié)
        return tab;
    }

    else {
        int n2 = n/2;
        
        int tab1[n2]; // première moitié de tab
        int tab2[n-n2]; // seconde moitié de tab

        for (int i=0; i<n2; i++) {
            tab1[i]=tab[i];
        }


        for (int i=n2; i<n; i++) {
            tab2[i-n2]=tab[i];
        }

        return merge(merge_sort(tab1, n2), n2, merge_sort(tab2, n-n2), n-n2); // récursif : on fusionne les tris des deux moitiés ! <3

    }

}

