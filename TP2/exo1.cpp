#include <iostream>
using namespace std;

void display_tab(int* tab, int n);
void selection_sort(int* tab, int n);


int main() {
    int tableau[6] = {5,2,8,7,15,2};
    display_tab(tableau, 6);

    selection_sort(tableau,6);
    display_tab(tableau, 6); // Ça marche !
    // {2, 2, 5, 7, 8, 15}

}



// Fonction qui affiche le tableau tab de taille n
void display_tab(int* tab, int n) {
    cout << "{" ;
    for (int j=0; j<n-1; j++) {
        cout << tab[j] << ", " ;
    }
    cout << tab[n-1] << "}" << endl;
}

// Tri par sélection
void selection_sort(int* tab, int n) {

    for (int i=0; i<n; i++) { // on parcourt la liste avec o

        int min = tab[i];
        int idmin = i;

        for (int j=i+1; j<n; j++) { // on parcourt la liste à partir de i

            if (tab[j] < min) { // si l'élement courant est inférieur au minimium
                min = tab[j]; // il devient le nveau minimum
                idmin = j;
            }
        }

    // échange min et élément courant
        tab[idmin] = tab[i]; 
        tab[i] =  min;
    }
}